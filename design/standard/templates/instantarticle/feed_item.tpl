{def $content_header = ezini(concat('FeedFields_', $node.object.class_identifier), 'ContentHeader', 'instantarticle.ini')}
{def $content_image = ezini(concat('FeedFields_', $node.object.class_identifier), 'ContentImage', 'instantarticle.ini')}
{def $content_image_alias = ezini(concat('FeedFields_', $node.object.class_identifier), 'ContentImageAlias', 'instantarticle.ini')}
{def $content_body = ezini(concat('FeedFields_', $node.object.class_identifier), 'ContentBody', 'instantarticle.ini')}
{def $content_footer = ezini(concat('FeedFields_', $node.object.class_identifier), 'ContentFooter', 'instantarticle.ini')}

<!doctype html>
<html lang="en" prefix="op: http://media.facebook.com/op#">
<head>
    <meta charset="utf-8">
    <link rel="canonical" href={$node.url_alias|ezurl('', 'full')}>
    <meta property="op:markup_version" content="v1.0">
</head>
<body>
<article>
    {if $content_header|ne('')}
        {if $content_header|contains('~')}
            {def $header_identifier_array = $content_header|explode('~')}
        {else}
            {def $header_identifier_array = array( $content_header )}
        {/if}

        <header>

            <h1>{foreach $header_identifier_array as $header_identifier}
                {attribute_view_gui attribute=$node.data_map.$header_identifier}
            {/foreach}</h1>

            <time class="op-published" datetime="{$node.object.published|datetime('custom', '%c')}">{$node.object.published|datetime('custom', '%F %d, %H:%i')}</time>
            <time class="op-modified" datetime="{$node.object.modified|datetime('custom', '%c')}">{$node.object.modified|datetime('custom', '%F %d, %H:%i')}</time>
        </header>
    {/if}

    {if $content_image|ne('')}
        {if $content_image|contains('~')}
            {def $image_identifier_array = $content_image|explode('~')}
        {else}
            {def $image_identifier_array = array( $content_image )}
        {/if}

        {foreach $image_identifier_array as $image_identifier}
            <img src={$node.data_map.$image_identifier.content.$content_image_alias.url|ezurl('', 'full')} />
        {/foreach}
    {/if}

    {if $content_body|ne('')}
        {if $content_body|contains('~')}
            {def $body_identifier_array = $content_body|explode('~')}
        {else}
            {def $body_identifier_array = array( $content_body )}
        {/if}

        {foreach $body_identifier_array as $body_identifier}
            {attribute_view_gui attribute=$node.data_map.$body_identifier}
        {/foreach}
    {/if}

    {if $content_footer|ne('')}
        {if $content_footer|contains('~')}
            {def $footer_identifier_array = $content_footer|explode('~')}
        {else}
            {def $footer_identifier_array = array( $content_footer )}
        {/if}

        <footer>
            {foreach $footer_identifier_array as $footer_identifier}
                {attribute_view_gui attribute=$node.data_map.$footer_identifier}
            {/foreach}
        </footer>
    {/if}
</article>
</body>
</html>