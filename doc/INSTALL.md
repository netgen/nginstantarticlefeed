# Netgen Instant Article Feed extension installation instructions

## Activating the extension

1. Extract the archive or clone this repo to `extension` folder of eZ Publish

2. Configure the extension in `instantarticle.ini`

    - Set the Facebook Pages meta key you obtained for the Instant Articles feature

    - Add the article-like classes you wish to display in the feed to the `AllowedClasses` list
    
    - Set the options for each article-like class
    
    - Set up item settings for the feed for each article-like class
    
3. Enable the extension in `site.ini/ExtensionSettings/ActiveExtensions`

4. Clear the caches and regenerate autoloads

## Setting up the functionality in the administration of the site

1. Add a checkbox with an identifier `show_on_facebook` to the allowed classes in the administration

2. Enable `instantarticle/feed` rights to the Anonymous users

## Changes in your custom extension

1. Include `design/standard/templates/instantarticle/fb_instant_article_code.tpl` to the <head> tag of your site

2. Create override files for each class configured in `design/yourdesign/templates/instantarticle/content_class_identifier.tpl`

3. The extension will only search for the article-like objects with `show_on_facebook` checkbox checked.



