Netgen Instant Article Feed
===========================

## About the extension

Netgen Instant Article Feed is an eZ Publish 4 extension that provides a way for eZ Publish to deliver article-like data to Facebook Instant Article API.

The extension creates an RSS 2.0 feed optimized for Instant Articles in Facebook.

The extension is licensed under [GNU GPLv2 license](LICENSE).

## Requirements

- eZ Publish 4.6+ / 2011.07+

- PHP 5.3.x

## License, installation instructions and changelog

[Installation instructions](doc/INSTALL.md)

[License](LICENSE)

[Configuration instructions](settings/instantarticle.ini)

[Changelog](doc/CHANGELOG.md)



