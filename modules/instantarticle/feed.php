<?php

/** @var eZModule $Module */
$Module = $Params['Module'];

eZURI::setTransformURIMode( 'full' );

//Fetch all article-like objects which have a Show on Facebook checkbox checked.

$instantArticleIni = eZINI::instance('instantarticle.ini');
$contentIni = eZINI::instance('content.ini');

if( $instantArticleIni->hasVariable('FeedSettings', 'AllowedClasses') )
{
    $contentClassIdentifierArray = $instantArticleIni->variable('FeedSettings', 'AllowedClasses');

    $articleNodes = array();

    foreach ( $contentClassIdentifierArray as $contentClassIdentifier )
    {
        $currentArticleNodes = eZContentObjectTreeNode::subTreeByNodeID(
            array(
                'ClassFilterType' => 'include',
                'ClassFilterArray' => array($contentClassIdentifier),
                'AttributeFilter' => array(
                    array( $contentClassIdentifier.'/show_on_facebook', '=', '1' )
                )
            ),
            $contentIni->variable('NodeSettings', 'RootNode')
        );

        $articleNodes = array_merge($articleNodes, $currentArticleNodes);
    }

    $feed = nGInstantArticleGenerator::createFeed($articleNodes);

    echo $feed->saveXML();
}
else
{
    echo 'Error creating feed';
}

eZExecution::cleanExit();
?>