<?php

class nGInstantArticleGenerator
{
    /**
     * @param array $articleNodes
     * @return DOMDocument
     */
    static function createFeed(array $articleNodes)
    {
        header( 'Content-Type: application/xml; charset=utf-8' );

        $locale = eZLocale::instance();

        $feed = new DOMDocument('1.0', 'utf-8');

        $instantArticleIni = eZINI::instance('instantarticle.ini');
        $siteIni = eZINI::instance();

        // Create RSS element.
        $rss = $feed->createElement('rss');
        $rss->setAttribute('version', '2.0');
        // This is necessary to use content:encoded tag for the items.
        $rss->setAttribute('xmlns:content', 'http://purl.org/rss/1.0/modules/content/');

        // channel is the container for the whole feed.
        $channel = $feed->createElement('channel');

        // The title is determined from [FeedSettings][Title] in instantarticle.ini.
        // If no title is entered, the default SiteName is used.
        if( $instantArticleIni->variable('FeedSettings', 'Title') !== '' )
        {
            $titleString = $instantArticleIni->variable('FeedSettings', 'Title');
        }
        else
        {
            $titleString = $siteIni->variable('SiteSettings', 'SiteName');
        }

        $title = $feed->createElement('title', $titleString);

        // The link is determined from [FeedSettings][Link] in instantarticle.ini.
        // If no link is entered, the default SiteURL is used.
        $url = 'http://';

        if ( $instantArticleIni->variable('FeedSettings', 'Link') !== '' )
        {
            $url .= $instantArticleIni->variable('FeedSettings', 'Link') . '/';
        }
        else
        {
            $url .= $siteIni->variable('SiteSettings', 'SiteURL') . '/';
        }

        $link = $feed->createElement('link', $url);

        $descriptionString = $instantArticleIni->variable('FeedSettings', 'Description');

        $description = $feed->createElement('description', $descriptionString);
        $language = $feed->createElement('language', $locale->httpLocaleCode());
        $lastBuildDate = $feed->createElement('lastBuildDate', date(DateTime::ISO8601, time()));

        $channel->appendChild($title);
        $channel->appendChild($link);
        $channel->appendChild($description);
        $channel->appendChild($language);
        $channel->appendChild($lastBuildDate);

        foreach( $articleNodes as $articleNode )
        {
            $channel->appendChild( nGInstantArticleGenerator::createItem($feed, $articleNode) );
        }

        $rss->appendChild($channel);
        $feed->appendChild($rss);

        return $feed;
    }

    /**
     * @param DOMDocument $feed
     * @param eZContentObjectTreeNode $nodeObject
     * @return DOMElement
     */
    private static function createItem( DOMDocument $feed, eZContentObjectTreeNode $nodeObject )
    {
        $instantArticleIni = eZINI::instance('instantarticle.ini');

        $item = $feed->createElement('item');

        $contentClassIdentifier = $nodeObject->object()->contentClassIdentifier();

        foreach( $instantArticleIni->variable('FeedFields_' . $contentClassIdentifier, 'Field') as $itemFieldName => $itemFieldConfiguration )
        {
            $itemField = $feed->createElement($itemFieldName, self::extractValue($nodeObject, $itemFieldConfiguration) );

            if ($itemFieldName == 'link')
            {
                if( $instantArticleIni->variable('FeedFields_' . $contentClassIdentifier, 'GenerateGuid') == 'yes' )
                {
                    $guid = $feed->createElement('guid', self::extractValue($nodeObject, $itemFieldConfiguration));
                    $item->appendChild($guid);
                }
            }


            $item->appendChild($itemField);
        }

        $tpl = eZTemplate::factory();

        $tpl->setVariable('node', $nodeObject);

        $templateResult = $tpl->fetch('design:instantarticle/feed_item.tpl');

        $cData = $feed->createCDATASection($templateResult);

        $content = $feed->createElement('content:encoded');
        $content->appendChild($cData);

        $item->appendChild($content);

        return $item;
    }

    /**
     * @param eZContentObjectTreeNode $nodeObject
     * @param string $itemFieldConfiguration
     * @return string
     */
    private static function extractValue(eZContentObjectTreeNode $nodeObject, string $itemFieldConfiguration)
    {
        $fieldFill = self::extractFieldFillTerms($itemFieldConfiguration);

        $value = '';

        if( count($fieldFill) > 1 )
        {
            foreach( array_reverse($fieldFill) as $currentConfiguration )
            {
                $method = self::extractFieldFillMethod($currentConfiguration);

                $methodName = 'extract' . ucfirst($method['method']) . 'Value';

                $value = self::$methodName( $nodeObject, $method['attribute'] );
            }

        }
        else
        {
            $method = self::extractFieldFillMethod($fieldFill[0]);

            $methodName = 'extract' . ucfirst($method['method']) . 'Value';

            $value = self::$methodName( $nodeObject, $method['attribute'] );
        }

        return $value;
    }

    /**
     * @param eZContentObjectTreeNode $nodeObject
     * @param $attribute
     * @return mixed
     */
    private static function extractNodeValue( eZContentObjectTreeNode $nodeObject, $attribute )
    {
        if($attribute == 'url_alias')
        {
            $instantArticleIni = eZINI::instance('instantarticle.ini');
            $siteIni = eZINI::instance();

            // The link is determined from [FeedSettings][Link] in instantarticle.ini.
            // If no link is entered, the default SiteURL is used.
            $url = 'http://';

            if ( $instantArticleIni->variable('FeedSettings', 'Link') !== '' )
            {
                $url .= $instantArticleIni->variable('FeedSettings', 'Link') . '/';
            }
            else
            {
                $url .= $siteIni->variable('SiteSettings', 'SiteURL') . '/';
            }

            $value = $url . $nodeObject->attribute($attribute);
        }
        else
        {
            $value = $nodeObject->attribute($attribute);
        }

        return $value;
    }

    /**
     * @param eZContentObjectTreeNode $nodeObject
     * @param $attribute
     * @return bool|string
     */
    private static function extractAttributeValue( eZContentObjectTreeNode $nodeObject, $attribute )
    {
        $object = $nodeObject->object();

        $dataMap = $object->dataMap();

        $contentObjectAttribute = $dataMap[$attribute];

        $contentObjectAttributeDataType = $contentObjectAttribute->attribute('data_type_string');

        $value = '';

        switch ($contentObjectAttributeDataType)
        {
            case 'ezdatetime':
            case 'ezdate':
                $value = date(DateTime::ISO8601, $contentObjectAttribute->toString());
                break;
            default:
                $value = $contentObjectAttribute->toString();
                break;
        }

        return $value;
    }

    /**
     * @param eZContentObjectTreeNode $nodeObject
     * @param $attribute
     * @return bool|mixed|string
     */
    private static function extractObjectValue( eZContentObjectTreeNode $nodeObject, $attribute )
    {
        $object = $nodeObject->object();

        $value = $object->attribute($attribute);

        if( !empty($value) ){
            if ( $attribute == 'published' )
            {
                $value = date(DateTime::ISO8601, $value);
            }

            if ( $attribute == 'owner' )
            {
                $value = $value->attribute('name');
            }
        }
        return $value;
    }

    /**
     * @param eZContentObjectTreeNode $nodeObject
     * @param string $attribute
     * @return string
     */
    private static function extractStringValue( eZContentObjectTreeNode $nodeObject, $attribute )
    {
        return $attribute;
    }

    /**
     * @param string $itemFieldConfigurationString
     * @return array
     */
    private static function extractFieldFillTerms( string $itemFieldConfigurationString )
    {
        return explode('|', $itemFieldConfigurationString);
    }

    /**
     * @param string $itemFieldConfigurationString
     * @return array
     */
    private static function extractFieldFillMethod( string $itemFieldConfigurationString )
    {
        $itemFieldConfigurationArray = explode(':', $itemFieldConfigurationString);

        if (count($itemFieldConfigurationArray) > 1)
        {
            return array(
                'method' => $itemFieldConfigurationArray[0],
                'attribute' => $itemFieldConfigurationArray[1]
            );
        }
        else return false;
    }
}
